# Certificate 

This project is for the "Generation Of Certificate Achievement" for the Orchids International School.

**Requirements:**
 
 - HTML file
 - CSS file
 - JavaScript file
 - Live Server

**Execution**

- To run the project open the Visual Studio Code.
- Open the index.html file and run the live server.
- Then the HTML web page will be opened for generating the Certificate for Achievement.
- Enter the name and generate your certificate.
- For generating certificate we have index.js and filesaver.js
- Finally the certificate will be downloaded in your device automatically.


